FROM haskell:8

WORKDIR /opt/app

COPY . /opt/app
RUN ghc -o hello hello.hs

# comment when only build is needed
CMD ["/opt/app/hello"]

